import Ember from 'ember';

/**
 * Support for form submission to the server API.
 * assumes use of ember-validations
 */
export default Ember.Mixin.create({
    /**
     * Is the form currently being submitted?
     */
    submitting: false,
    
    /**
     * Has user tried submitting this form at any point? Don't show errors
     * until this is the case
     */
    hasSubmitted: false,
    
    /**
     * Errors from the API
     */
    apiErrors: {},
    
    /**
     * Reset the form fields handled by the mixin.
     * Subclasses using this should call: `this._super.apply(this, arguments);`
     */
    reset: function() {
        this.set('submitting', false);
        this.set('hasSubmitted', false);
        this.set('apiErrors', {});
    },
    
    /**
     * Show Error messages on form groups? 
     * Only do so if form has been submitted
     */
    showErrorMessage: Ember.computed('hasSubmitted', function() {
        return this.get('hasSubmitted');
    }),
    
    /**
     * Error message on API not specific to any field
     */
    nonFieldErrors: Ember.computed(
        'apiErrors.non_field_errors.[]', 'apiErrors.detail.[]', function() {
            var errors = this.combinedErrorMessages(
                [], this.get('apiErrors.non_field_errors'));
            return this.combinedErrorMessages(
                errors, this.get('apiErrors.detail'));
        }),
    
    /**
     * Pick one error message from the list of non-field error messages
     */
    nonFieldErrorMessage: Ember.computed('nonFieldErrors', function() {
        var errors = this.get('nonFieldErrors'),
            messages = [],
            index;
        
        errors.forEach(function(error) {
            messages.push(error);
        });
        index = Math.floor(Math.random() * messages.length);
        return messages[index];
    }),
    
    /**
     * Combine the local and REST API errors for a given field
     * 
     * @param localErrors   Array of local validator errors for a field
     * @param apiErrors     Array of REST API errors for a field
     *
     * @return combined array of error messasges
     */
    combinedErrorMessages: function(localErrors, apiErrors) {
        var errors = [];
        
        if (localErrors && localErrors.constructor === Array) {
            errors.addObjects(localErrors);
        }
        if (apiErrors && apiErrors.constructor === Array) {
            errors.addObjects(apiErrors);
        }
        
        return errors;
    },
    
    /**
     * Get an array of Data model errors for a given `model.errors.property` 
     * value.
     *
     * @param dataErrors an array of objects with the keys `attribute` and 
     *     `message`
     */
    dataErrorMessages: function(dataErrors) {
        var errorMessages = [];
        if (!Ember.isBlank(dataErrors)) {
            for (var i=0; i< dataErrors.length; i++) {
                errorMessages.push(dataErrors[i].message);
            }
        }
        return errorMessages;
    }
});
