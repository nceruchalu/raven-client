import Ember from 'ember';

/**
 * Additional functionality to be used on text input and text area fields
 */
export default Ember.Mixin.create({
    /**
     * Should the text input be selected during a click event
     */
    selectOnClick: false,
    
    /**
     * Stop event propagation when pressing "enter".
     *
     * Most useful in the case when undesired (global) keyboard shortcuts are 
     * getting triggered while interacting with this particular input element.
     */
    stopEnterKeyDownPropagation: false,
    
    click(event) {
        if (this.get('selectOnClick')) {
            event.currentTarget.select();
        }
    },

    keyDown(event) {
        if (this.get('stopEnterKeyDownPropagation') && event.keyCode === 13) {
            event.stopPropagation();
            return true;
        }
    }
});
