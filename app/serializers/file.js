import DRFSerializer from './drf';

export default DRFSerializer.extend({
    primaryKey: 'hash_key'
});
