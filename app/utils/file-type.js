import Ember from 'ember';

/**
 * Determine file format from its MIME type
 *
 * @param type file MIME type
 *
 * @returns a format string that can be used with font-awesome
 *     to generate icons of the form fa-file-{format}-o
 *
 * @ref http://www.freeformatter.com/mime-types-list.html
 */
function fileTypeByMime(type) {
    var format = '';

    if (type.match('^image\/')) {
        format = 'image';

    } else if (type.match('^video\/')) {
        format = 'video';

    } else if (type.match('^text\/')) {
        format = 'text';
        
    } else if (type.match('^audio\/')) {
        format = 'audio';
        
    } else if (type === 'application/pdf') { 
        format = 'pdf';
        
    } else if (['text/vnd.curl.scurl', // .scurl
                 'application/java-vm', // .class
                 'application/javascript', // .js
    ].contains(type)) {
        format = 'code';

    } else if ([
        'application/x-7z-compressed', // .7z
        'application/x-rar-compressed', // .rar
        'application/zip', // .zip
        'application/x-tar', // .tar
        'application/x-gtar', // .gtar
        'application/x-bzip2', // .bz2
        'application/gzip', // .gz
        'application/x-lzip', // .lz
        'application/x-lzma', // .lzma
        'application/x-lzop', // .lzo
        'application/x-compress', // .z
    ].contains(type)) {
        format = 'archive';

    } else if ([
        'application/vnd.kde.kword', // .kwd
        'application/vnd.lotus-wordpro', // .lwp
        'application/vnd.ms-word.document.macroenabled.12', // .docm
        'application/vnd.ms-word.template.macroenabled.12', // .dotm
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document', // .docx
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template', // .dotx
        'application/msword', // .doc
        'application/x-mswrite', // .wri
        'application/vnd.wordperfect', // .wpd
    ].contains(type)) {
        format = 'word';

    } else if ([
        'application/vnd.ms-powerpoint', // .ppt
        'application/vnd.ms-powerpoint.addin.macroenabled.12', // .ppam
        'application/vnd.ms-powerpoint.slide.macroenabled.12', // .sldm
        'application/vnd.ms-powerpoint.presentation.macroenabled.12', // .pptm
        'application/vnd.ms-powerpoint.slideshow.macroenabled.12', // .ppsm
        'application/vnd.ms-project', // .mpp
        'application/vnd.ms-powerpoint.template.macroenabled.12', //. potm
        'application/vnd.openxmlformats-officedocument.presentationml.presentation', // .pptx
    ].contains(type)) {
        format = 'powerpoint';

    } else if ([
        'application/vnd.ms-excel', // .xls
        'application/vnd.ms-excel.addin.macroenabled.12', // .xlam
        'application/vnd.ms-excel.sheet.binary.macroenabled.12', //.xlsb
        'application/vnd.ms-excel.template.macroenabled.12', // .xitm
        'application/vnd.ms-excel.sheet.macroenabled.12', // .xlsm
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // .xslx
    ].contains(type)) {
        format = 'excel';
    }
      

    return format;
}

/**
 * Determine file format from its name
 *
 * @param name file name
 *
 * @returns a format string that can be used with font-awesome
 *     to generate icons of the form fa-file-{format}-o
 *
 * @ref http://www.freeformatter.com/mime-types-list.html
 */
function fileTypeByName(name) {
    var format = '';
        
    // Does the given extension end the file name?
    function usedAsExtension(extension) {
        return name.match('.'+extension+'$');
    }
    
    if (['dxf', 'bmp', 'btif', 'sub', 'ras', 'cgm', 'cmx', 'uvi', 'djvu', 'dwg',
         'mmr', 'rlc', 'xif', 'fst', 'fbs', 'fpx', 'npx', 'fh', 'g3', 'gif',
         'ico', 'ief', 'jpeg', 'jpg', 'mdi', 'ktx', 'pcx', 'psd', 'pic', 'pnm',
         'pbm', 'pgm', 'png', 'ppm', 'svg', 'rgb', 'tiff', 'wbmp', 'webp',
         'xbm', 'xpm', 'xwd'
    ].any(usedAsExtension)) {
        format = 'image';
             
    } else if (['3gp', '3g2', 'uvh', 'uvm', 'uvu', 'uvp', 'uvs', 'uvv', 'fvt',
                'f4v', 'flv', 'fli', 'h261', 'h263', 'h264', 'jpm', 'jpgv',
                'm4v', 'asf', 'pyv', 'wm', 'wmx', 'wmv', 'wvx', 'mj2', 'mxu',
                'mpeg', 'mp4', 'ogv', 'webm', 'qt', 'movie', 'viv'
    ].any(usedAsExtension)) {
        format = 'video';
        
    } else if (['s', 'par', 'csv', 'curl', 'dcurl', 'mcurl', 'flx', 'gv', 'ics',
                '3dml', 'spot', 'jad', 'fly', 'n3', 'dsc', 'rtx', 'etx', 'sgml',
                'tsv', 'txt', 't', 'ttl', 'uri', 'uu', 'vcs', 'vcf', 'wml',
                'wmls', 'yaml', 'json', 'md',
    ].any(usedAsExtension)) {
        format = 'text';

    } else if (['adp', 'aac', 'aif', 'uva', 'eol', 'dra', 'dts', 'dtshd',
                'rip', 'lvp', 'm3u', 'pya', 'wma', 'wax', 'mid', 'mp4a',
                'ecelp4800', 'ecelp7470', 'ecelp9600', 'oga', 'weba', 'ram',
                'rmp', 'au', 'wav'
    ].any(usedAsExtension)) {
        format = 'audio';
        
    } else if (['pdf'].any(usedAsExtension)) {
        format = 'pdf';
        
    } else if (['c', 'cpp', 'cc', 'cxx', 'cs', 'asmx', 'js', 'jse', 'html',
                'htm', 'asp', 'hta', 'aspx', 'm', 'mm', 'h', 'hh', 'hpp', 'uc',
                'as', 'mxml', 'java', 'jav', 'class', 'j', 'jsl', 'D', 'php',
                'py', 'rb', 'asm', 'css', 'hss', 'sass', 'scss', 'less', 'ccss',
                'pcss', 'html.erb', 'erb', 'rjs', 'coffee', 'cfm', 'cu', 'rs',
                'rs.in', 'sql'
        ].any(usedAsExtension)) {
        format = 'code';
        
    } else if (['7z', 'rar', 'zip', 'tar', 'gtar', 'bz2', 'gz', 'lz', 'lzma',
                'lzo', 'z'
    ].any(usedAsExtension)) {
        format = 'archive';
        
    } else if (['kwd', 'lwp', 'docm', 'dotm', 'docx', 'dotx', 'doc', 'wri',
                'wpd', 'pages'
    ].any(usedAsExtension)) {
        format = 'word';
        
    } else if (['ppt', 'pptx', 'ppam', 'sldm', 'ppsm', 'mpp', 'potm', 'numbers'
    ].any(usedAsExtension)) {
        format = 'powerpoint';

    } else if (['xls', 'xlsx', 'xlam', 'xlsb', 'xitm', 'xlsm', 'keynote'
    ].any(usedAsExtension)) {
        format = 'excel';
    }

    return format;
}

/**
 * Determine file format from its MIME type and name
 *
 * @param name file name
 * @param type file MIME type
 *
 * @returns a format string that can be used with font-awesome
 *     to generate icons of the form fa-file-{format}-o
 *
 * @ref http://www.freeformatter.com/mime-types-list.html
 */
export default function fileType(name, type) {
    // First try to get the format from the name ad if that fails attempt
    // extracting it from the mime type
    var format = fileTypeByName(name.toLowerCase());
    
    if (Ember.isBlank(format)) {
        format = fileTypeByMime(type.toLowerCase());
    }
    return format;
}
