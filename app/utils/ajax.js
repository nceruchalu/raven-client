import Ember from 'ember';
import Config from '../config/environment';
import request from 'ic-ajax';

/**
 * Custom $.ajax method. This allows us to use relative REST API URL paths,
 * which will be converted to absolute paths based off of the base URL.
 *
 * Ref: https://github.com/discourse/discourse/blob/master/app/assets/javascripts/discourse/mixins/ajax.js
 */
export default function ajax() {
    var baseURL = Config.APP.API_HOST + '/' + Config.APP.API_NAMESPACE + '/',
        defaultArgs = {dataType:    'json',
                       contentType: 'application/json; charset=utf-8'},
        url,  // relative URL
        args; // all other ajax arguments
    
    if (arguments.length === 1) {
        if (typeof arguments[0] === 'string') {
            url = arguments[0];
            args = {};
        } else {
            args = arguments[0];
            url = args.url;
        }
    } else if (arguments.length === 2) {
        url = arguments[0];
        args = arguments[1];
    }
    
    // convert relative url to absolute URL
    url = baseURL + url;
    args.url = url;
    
    // Ensure method is upper case
    args.method = args.method.toUpperCase();
    
    // Ensure the default argument keys are used when not specified in args
    Ember.$.extend(defaultArgs, args);
    args = defaultArgs;
    
    // When the datatype is json and data is provided stringify it to 
    // ensure jQuery doesn't attempt to URLEncode the data
    if (args.dataType === 'json' && !Ember.isBlank(args.data)) {
        args.data = JSON.stringify(args.data);
    }
    
    return request(args);
}
