import Ember from 'ember';

/**
 * Generate a singularized/pluralized version of a string based on a 
 * count. If the count is 1 then this generates the singularized version,
 * else it generates the pluralized version
 * > applyInfection('file', 1)
 *   'file'
 * > applyInfection('file', 2)
 *   'files'
 * 
 * @param string string to be singularized/pluralized
 * @param count reference count
 *
 * @return formatted string
 */
export default function applyInflection(string, count) {
    var inflector = new Ember.Inflector(Ember.Inflector.defaultRules),
        result;
    if (count === 1) {
        result = inflector.singularize(string);
    } else {
        result = inflector.pluralize(string);
    }
    return result;
}
