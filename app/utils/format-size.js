/**
 * Format an integer byte size to a human readable string.
 * > formatSize(1024)
 *   '1.0KB'
 * 
 * @param num byte size to be formatted
 *
 * @return string version of formatted byte size
 */
export default function formatSize(num) {
    var units = ['B', 'KB', 'MB', 'GB'];
    for (var i=0; i< units.length; i++) {
        if (num < 1024) {
            return num.toFixed(units[i] ? 1 : 0).toString() + units[i];
        }
        num /= 1024;
    }
    return num.toFixed(1).toString() + 'TB';
}
