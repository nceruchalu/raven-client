import Config from '../config/environment';

/**
 * Generate the absolute REST API path given a relative path
 *
 * @param relativePath relative path, starting without a leading slash
 */
export default function apiPath(relativePath) {
    var baseURL = Config.APP.API_HOST + '/' + Config.APP.API_NAMESPACE + '/';
    return baseURL + relativePath;
}
