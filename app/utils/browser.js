/**
 * Parse the navigator.userAgent and export it as a browser object
 */

/**
 * Parse user agent string
 *
 * @param e user agent string
 * @return browser data
 */
function parseUserAgent(userAgent) {
    userAgent = userAgent.toLowerCase();
    var browser = {
        android: /android/.test(userAgent),
        chrome: !1,
        linux: /linux/.test(userAgent),
        mac: /mac/.test(userAgent),
        mozilla: !1,
        msie: !1,
        opera: !1,
        phone: /iphone/.test(userAgent),
        safari: !1,
        version: parseInt(
            (userAgent.match(/.+(?:rv|it|ra|ie|edge)[\/: ]([\d.]+)/) || [])[1], 
            10),
        windows: /windows/.test(userAgent)
    };
    
    if (/(trident|msie|edge)/.test(userAgent)) {
        browser.msie = !0;
        
    } else if (/webkit/.test(userAgent)) {
        if (/chrome/.test(userAgent)) {
            browser.chrome = !0;
            browser.version = parseInt(
                (userAgent.match(/chrome\/([\d.]+)/) || [])[1], 10);
            
        } else {
            browser.safari = !0;
            browser.version = parseInt(
                (userAgent.match(/version\/([\d.]+)/) || [])[1], 10);
        }
        
    } else { 
        if(/ opera / .test(userAgent)) {
            browser.opera = !0;
            
        } else if (/mozilla/.test(userAgent) && !/compatible/.test(userAgent)) {
            browser.mozilla = !0;
        }
    }
    
    return browser;
}

export default parseUserAgent(navigator.userAgent.toLowerCase());
