import Ember from 'ember';
import config from './config/environment';
import documentTitle from 'raven/utils/document-title';

const Router = Ember.Router.extend({
    // Use HTML5 History API with trailing-slash URLs instead of hash-tag URLs
    location: 'trailing-history',
    rootURL: config.rootURL
});

documentTitle();

Router.map(function() {
    // Homepage
    this.route('home', { path: '/' }, function() {
        this.route('index', { path: '/' });
        this.route('forgot', { path: '/forgot' });
        this.route('privacy', { path: '/privacy' });
        this.route('pricing', { path: '/pricing' });
        this.route('signin', { path: '/signin' });
        this.route('signout', { path: '/signout' });
        this.route('signup', { path: '/signup' });
        this.route('terms', { path: '/terms' });
    });
    
    // General 404 error page
    this.route('error404', { path: '/*path' });
});

export default Router;
