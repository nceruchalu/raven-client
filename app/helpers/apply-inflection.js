import Ember from 'ember';
import applyInflectionUtil from 'raven/utils/apply-inflection';

/**
 * Generate a singularized/pluralized version of a string based on a 
 * count. If the count is 1 then this generates the singularized version,
 * else it generates the pluralized version
 * > applyInfection('file', 1)
 *   'file'
 * > applyInfection('file', 2)
 *   'files'
 * 
 * @param string string to be singularized/pluralized
 * @param count reference count
 *
 * @return formatted string
 */
export function applyInflection([string, count]/*, hash*/) {
    return applyInflectionUtil(string, count);
}

export default Ember.Helper.helper(applyInflection);
