import Ember from 'ember';
import formatSizeUtil from 'raven/utils/format-size';

/**
 * Format an integer byte size to a human readable string.
 * > formatSize(1024)
 *   '1.0KB'
 * 
 * @param num byte size to be formatted
 *
 * @return string version of formatted byte size
 */
export function formatSize([num]) {
    return num ? formatSizeUtil(num) : 0;
}

export default Ember.Helper.helper(formatSize);
