import DS from 'ember-data';
import Ember from 'ember';
import Config from 'raven/config/environment';

export default DS.Model.extend({

    hashKey: DS.attr('string'),
    name: DS.attr('string'),
    size: DS.attr('number', {defaultValue: 0}),
    contentType: DS.attr('string'),
    createdAt: DS.attr('date'),
    updatedAt: DS.attr('date'),
    meta: DS.attr(),
    uploadedFile: DS.attr('boolean', {defaultValue: false}),
        
    
    // Local properties
    uploadStatus: Config.FILE.STATUS.READY,
    progress: 0, // Range from 0 to 100

    /**
     * File can only be edited before it starts uploading
     * or after it fails
     */
    isEditable: Ember.computed('uploadStatus', function() {
        return ![Config.FILE.STATUS.UPLOADING,
                 Config.FILE.STATUS.COMPLETED].contains(
                     this.get('uploadStatus'));
    }),
    
    /**
     * Is the status an error status?
     */
    hasError: Ember.computed('uploadStatus', function(){
        return [Config.FILE.STATUS.DUPLICATE_NAME,
                Config.FILE.STATUS.LONG_NAME,
                Config.FILE.STATUS.LARGE,
                Config.FILE.STATUS.MAKES_TOTAL_LARGE,
                Config.FILE.STATUS.CREDENTIALS_FAILED,
                Config.FILE.STATUS.FAILED].contains(
                    this.get('uploadStatus'));
    }),

    /**
     * Is the status one that requires a warning?
     */
    hasWarning: Ember.computed('uploadStatus', function(){
        return [Config.FILE.STATUS.EMPTY].contains(
            this.get('uploadStatus'));
    }),

    /**
     * Indicator of if the original file upload is completed
     */
    isCompleted: Ember.computed.gte('uploadStatus',
                                    Config.FILE.STATUS.COMPLETED),
});
