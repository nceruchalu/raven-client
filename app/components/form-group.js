import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['form-group', 'formGroup'],
    classNameBindings: ['errorClass'],
    showErrorMessage: true,
    errors: null,
    
    /**
     * Does the form group have one or more errors?
     */
    hasError: Ember.computed('errors.[]', function() {
        var errors = this.get('errors');
        return !Ember.isEmpty(errors);
    }),
    
    /**
     * CSS class that used to indicate if the form group has errors or is valid
     */
    errorClass: Ember.computed('hasError', function() {
        var formGroupClass = '';
        
        if (this.get('showErrorMessage')) {
            formGroupClass = 'formGroup';
            var classModifier = this.get('hasError') ? 'error' : 'success';
            formGroupClass += '--' + classModifier;
        }
        return formGroupClass;
    }),
    
    /**
     * Error message to be shown if the form group has one or more errors.
     * If there are multiple errors, show 1 at random
     */
    errorMessage: Ember.computed('errors.[]', function() {
        var errors = this.get('errors'),
            messages = [],
            index;
        
        errors.forEach(function(error) {
            messages.push(error);
        });
        index = Math.floor(Math.random() * messages.length);
        return Ember.String.htmlSafe(messages[index]);
    })
});
