import Ember from 'ember';
import fileType from 'raven/utils/file-type';
import formatSize from 'raven/utils/format-size';
import Config from 'raven/config/environment';

/**
 * Representation of a file in a list of files
 */
export default Ember.Component.extend({
    tagName: 'tr',
    classNames: ['file'],

    /**
     * Component's model. The file to be displayed
     */
    file: null,

    /**
     * Are we currently in the middle of the final file(s) transfer?
     */
    isTransferring: false,

    /**
     * CSS representation of progress witdh
     */
    progressStyle: Ember.computed('file.progress', function() {
        var progress  = this.get('file.progress'),
            style = `width: ${progress}%`;
        
        return Ember.String.htmlSafe(style);
    }),

    /**
     * File's mime type classification
     */
    formattedType: Ember.computed('file.name', 'file.contentType', function() {
        return fileType(this.get('file.name'), this.get('file.contentType')) ||
               'generic';
    }),

    /**
     * Description of file's upload status
     */
    formattedStatus: Ember.computed('file.uploadStatus', 'isUploading', function() {
        var description = 'Unknown',
            status = this.get('file.uploadStatus'),
            formattedMaxSize = formatSize(Config.FILE.MAX_SIZE),
            formattedMaxTotalSize = formatSize(Config.FILE.MAX_TOTAL_SIZE);
        
        if (status === Config.FILE.STATUS.READY) {
            description = 'Processing...';

        } else if (status === Config.FILE.STATUS.DUPLICATE_NAME) {
            description = 'Another file already has this name. '+
                          'Maybe try separate uploads?';

        } else if (status === Config.FILE.STATUS.LONG_NAME) {
            description = 'Name exceeds ' + Config.FILE.MAX_NAME_LENGTH +
                          ' character limit. Try renaming it.';
            
        } else if (status === Config.FILE.STATUS.LARGE) {
            description = 'Size exceeds ' + formattedMaxSize + ' limit';

        } else if (status === Config.FILE.STATUS.MAKES_TOTAL_LARGE) {
            description = 'Makes transfer exceed ' + formattedMaxTotalSize +
                          ' limit,';

        } else if (status === Config.FILE.STATUS.EMPTY) {
            description = 'Ready but file seems to be empty.';

        } else if (status === Config.FILE.STATUS.CREDENTIALS_FAILED) {
            description = 'Could not pre-process file.';

        } else if (status === Config.FILE.STATUS.HAS_CREDENTIALS) {
            description = 'Ready to start uploading';
            
        } else if (status === Config.FILE.STATUS.UPLOADING) {
            description = 'Uploading';

        } else if (status === Config.FILE.STATUS.FAILED) {
            description = 'Upload failed';
            
        } else if (status >= Config.FILE.STATUS.COMPLETED) {
            if (this.get('isUploading')) {
                description = 'Transferring...';
            } else {
                description = 'Ready to transfer';
            }
        }
        return description;
    }),
    
    actions: {
        /**
         * Forward this action and model to the controller/parent component
         */
        remove: function() {
            this.sendAction('remove', this.get('file'));
        }
    }
});
