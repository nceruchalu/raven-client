import Ember from 'ember';
import TextInputMixin from 'raven/mixins/text-input';

/**
 * Custom input component that doesn't propagate the enter event
 */
export default Ember.TextField.extend(TextInputMixin, {
    stopEnterKeyDownPropagation: true
});
