import Ember from 'ember';
import Config from 'raven/config/environment';
import FormSubmission from 'raven/mixins/form-submission';
import apiPath from 'raven/utils/api-path';
import ajax from 'raven/utils/ajax';
import emailRegex from 'raven/utils/email-regex';
import browser from 'raven/utils/browser';
import EmberValidations from 'ember-validations';
/* global Clipboard */
/* global JSZip */

/**
 * Component for use in uploading files
 *
 * Ref: https://github.com/blueimp/jQuery-File-Upload/blob/master/js/jquery.fileupload-ui.js
 */
export default Ember.Component.extend(EmberValidations, FormSubmission, {
    classNames: ['file-upload'],
    classNameBindings:['isShowingProgress:is-uploading'],

    /**
     * The store service
     */
    store: Ember.inject.service('store'),
    
    maxFilesSize: Config.FILE.MAX_TOTAL_SIZE,

    /**
     * Archive creation status
     */
    archiveStatus: Config.FILE.ARCHIVE_STATUS.NO_ARCHIVE,

    /**
     * Archive file content
     */
    archive: null,

    /**
     * Upload data object retrieved from the server
     */
    uploadData: null,

    /**
     * Current upload status.
     */
    transferStatus: Config.TRANSFER_STATUS.INACTIVE,
    
    /**
     * Collection of all files both pending upload and uploaded
     */
    files: [],

    /**
     * Email address that was successfully forwarded the transfer URL
     * during the request, (if any was provided)
     */
    successEmail: null,

    /**
     * Is the transfer URL currently being shared via email?
     */
    sendingEmail: false,
    
    /**
     * Are we currently in the middle of a transfer?
     */
    isTransferring: Ember.computed.equal('transferStatus',
                                         Config.TRANSFER_STATUS.STARTED),

    /**
     * Has a transfer failed?
     */
    transferFailed: Ember.computed.equal('transferStatus',
                                            Config.TRANSFER_STATUS.FAILED),
    
    /**
     * Has a transfer completed?
     */
    transferCompleted: Ember.computed.equal('transferStatus',
                                            Config.TRANSFER_STATUS.COMPLETED),

    /**
     * Transfer URL that can be shared
     */
    transferUrl: Ember.computed('uploadData', function() {
        var uploadData = this.get('uploadData'),
            host = Config.APP.WEB_HOST,
            hashKey,
            url;
        if (!Ember.isBlank(uploadData)) {
            hashKey = uploadData.hash_key;
            url = `${host}/d/${hashKey}/`;
        }
        return url;
    }),
    
    /**
     * Are there files to be displayed?
     */
    hasFiles: Ember.computed('files.[]', function() {
        return this.get('files.length') > 0;
    }),

    /**
     * Files with no errors
     */
    filesNoErrors: Ember.computed('files.@each.uploadStatus', function() {
        return this.get('files').filterBy('hasError', false);
    }),

    /**
     * Total size of all uploaded files
     */
    filesSize: Ember.computed('files.[]', function() {
        var fileSizes = this.get('files').mapBy('size');
        return fileSizes.reduce(
            function(previousValue, item) {
                return previousValue + item;
            },
            0);
    }),
    
    /**
     * Size of all error-free files
     */
    filesSizeNoError: Ember.computed('files.@each.uploadStatus', function() {
        var fileSizes = this.get('files').map(function(file) {
            return file.get('hasError') ? 0 : file.get('size');
        });
        return fileSizes.reduce(
            function(previousValue, item) { return previousValue + item; },
            0);
    }),
    
    /**
     * Count of files with errors
     */
    filesWithErrorsCount: Ember.computed(
        'files.[]', 'filesNoErrors.[]', function() {
            return this.get('files.length') - this.get('filesNoErrors.length');
        }
    ),

    /**
     * Is the total file size past the max size limit?
     */
    filesSizeIsExcessive: Ember.computed('filesSize', function() {
        return this.get('filesSize') > Config.FILE.MAX_TOTAL_SIZE;
    }),

    /**
     * Have all files completed processing?
     */
    allFilesCompleted:  Ember.computed('files.@each.uploadStatus', function() {
        return this.get('filesNoErrors').isEvery('isCompleted', true);
    }),
    
    /**
     * Disable the transfer button if there are no files to upload
     * or currently in the middle of an upload
     */
    transferDisabled: Ember.computed('isTransferring', 'filesNoErrors.[]', function() {
        return (this.get('filesNoErrors.length') === 0) ||
               this.get('isTransferring');
    }),
    
    /**
     * Monitor for changes to the collection of files (addition/removal)
     * and changes to uploadStatus property of any file to ensure they are
     * handled appropriately
     */
    filesChanged: Ember.observer('files.@each.uploadStatus', function() {
        var _this = this;
        
        this.get('files').forEach(function(file) {
            var newStatus =  _this._determineFileStatus(file);
            
            // Don't touch files that are currently saving
            if (file.get('isSaving')) {
                return;
            }
                        
            file.set('uploadStatus', newStatus);
            
            if (file.get('isNew') &&
                newStatus === Config.FILE.STATUS.READY) {
                    // A new file that is yet to be saved should be saved
                    // so we can get the metadata
                    _this._getFileMeta(file);
            }
        });
    }),
    
    /**
     * Monitor for when when all files have completed processing and transfer
     * has been started.
     *
     * On the first occurrence, we will be creating archive.
     * If the archive already exists, then we are back here because an upload
     * failed so we will try that again
     * 
     */
    archiveCreationTrigger: Ember.observer('allFilesCompleted', 'isTransferring', function() {
        var archiveNotStarted = (this.get('archiveStatus') ===
            Config.FILE.ARCHIVE_STATUS.NO_ARCHIVE);
        
        if (this.get('allFilesCompleted') && this.get('isTransferring')) {
            if (archiveNotStarted) {
                this._createArchive();
            } else {
                this._createUpload();
            }
        }
    }),
    
    /**
     * Merge client-side validation errors in this.get('errors') with
     * server-side validation errors in this.get('apiErrors).
     * Fields that are validated by both the client and the server, need to have
     * the server errors cleared out (by setting to an empty array) so we don't
     * keep repeating server-side errors when new client-side errors are
     * generated.
     */
    emailErrors: Ember.computed(
        'errors.email.[]', 'apiErrors.email.[]', function() {
            var errors =  this.combinedErrorMessages(
                this.get('errors.email'), this.get('apiErrors.email'));
            this.set('apiErrors.email', []);
            return errors;
        }),
    
    validations: {
        'email': {
            format: {
                allowBlank: true,
                with: emailRegex,
                message: 'Email format is name@domain.com'
            }
        }
    },

    /**
     * Email address field can only be updated if there's a valid email address
     * and we aren't in the middle of sending an email
     */
    canSendEmail: Ember.computed('sendingEmail', 'emailErrors', function() {
        return (Ember.isBlank(this.get('emailErrors')) &&
            !this.get('sendingEmail'));
    }),
    
    didInsertElement: function() {
        this._super(...arguments);
        
        Ember.run.next(this, function() {
            this._setupClipboard();
            this._setupFileUploader();
        });
    },

    willDestroy: function () {
        var clipboard = this.get('clipboard');
        this._super(...arguments);

        clipboard.destroy();
        this.set('clipboard', null);
    },

    /**
     * Setup clipboard functionality
     */
    _setupClipboard() {
        var clipboard = new Clipboard('.btn--copyUrl');
        this.set('clipboard', clipboard);

        Ember.$('.btn--copyUrl').tooltip({
            trigger: 'manual',
            placement: 'bottom'
        });
        
        function setTooltip(btn, message) {
            Ember.$(btn).attr('data-original-title', message)
                 .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                Ember.$(btn).tooltip('hide');
            }, 5000);
        }
        
        clipboard.on('success', function(e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
        });

        clipboard.on('error', function(e) {
            var commandKey = browser.mac ? '⌘' : 'Ctrl';
            setTooltip(e.trigger, 'Use ' + commandKey + '+C to copy');
            hideTooltip(e.trigger);
        });
    },
    
    /**
     * Create a new file to get its metadata, which contains the Amazon S3
     * upload credentials.
     *
     * @param file file whose metadata needs to be acquired
     */
    _getFileMeta: function(file) {
        file.save().then(function() {
            var fileUploadCredentials, fileUploadData;
            
            file.set('uploadStatus',
                     Config.FILE.STATUS.HAS_CREDENTIALS);
            if (!Ember.isBlank(file.get('meta'))) {
                // File just got its metadata so start uploading
                fileUploadCredentials = file.get('meta')['presigned_post'];
                fileUploadData = file.get('fileUploadData');
                fileUploadData.url = fileUploadCredentials.url;
                fileUploadData.formData = fileUploadCredentials.fields;
                fileUploadData.submit();
            }
            
        }).catch(function() {
            // Warn user that this isn't going to work
            file.set('uploadStatus',
                     Config.FILE.STATUS.CREDENTIALS_FAILED);
        });
    },
    
    /**
     * Determine the file upload status
     *
     * @param file file model object
     *
     * @returns a Config.FILE.STATUS dictionary key
     */
    _determineFileStatus: function(file) {
        var status,
            currentStatus = file.get('uploadStatus'),
            name = file.get('name'),
            size = file.get('size'),
            uploadId = file.get('uploadId'),
            possibleTotalSize = size,
            fileNames = [];
        
        // A file that's saving is trying to get the metadata so
        // leave it in whatever state it's in
        if (file.get('isSaving')) {
            return currentStatus;
        }

        // A file that's started the upload process doesn't get its status
        // changed.
        if (currentStatus >= Config.FILE.STATUS.CREDENTIALS_FAILED) {
            return currentStatus;
        }

        // Start by assuming the best case
        status = Config.FILE.STATUS.READY;
            
        // Loop through all files that come before this file to get the total
        // size and pre-existing file names
        this.get('files').forEach(function(item) {
            if (item.get('uploadId') < uploadId && !item.get('hasError')) {
                fileNames.push(item.get('name'));
                possibleTotalSize += item.get('size');
            }
        });

        if (fileNames.contains(name)) {
            // Duplicate file name
            status = Config.FILE.STATUS.DUPLICATE_NAME;

        } else if (name.length > Config.FILE.MAX_NAME_LENGTH) {
            // long file name
            status =  Config.FILE.STATUS.LONG_NAME;
            
        } else if (size > Config.FILE.MAX_SIZE) {
            // File is too large
            status = Config.FILE.STATUS.LARGE;
            
        } else if (possibleTotalSize > Config.FILE.MAX_TOTAL_SIZE) {
            // Adding this file to the upload collection pushes total past limit
            status = Config.FILE.STATUS.MAKES_TOTAL_LARGE;
            
        } else if (size === 0) {
            // Empty file
            status = Config.FILE.STATUS.EMPTY;
        }

        return status;
    },

    /**
     * Attempt creating upload archive
     */
    _createArchive: function() {
        var _this = this,
            archive = new JSZip(),
            files = this.get('filesNoErrors'),
            filesCount = files.length,
            readFilesCount = 0,
            reader;

        // This is only possible on browsers that suppport FileReader and
        // total files size of reasonable size and total files count justifies
        // this
        if (filesCount > 1 &&
            window.FileReader &&
            window.ArrayBuffer &&
            JSZip.support.blob &&
            this.get('filesSizeNoError') <=Config.FILE.MAX_TOTAL_ARCHIVE_SIZE) {
                this.set('archiveStatus',
                         Config.FILE.ARCHIVE_STATUS.ARCHIVE_CREATING);

                // Iterate through all files, read them all and add to a zip
                files.forEach(function(file) {
                    var dataFile = file.get('fileUploadData').files[0];

                    reader = new FileReader();
                    
                    reader.onload = function(event) {
                        archive.file(file.get('name'), event.target.result);
                        readFilesCount++;
                        
                        if (readFilesCount === filesCount) {
                            // all files have been read so create archive
                            archive.generateAsync({type: 'blob'})
                                   .then(function(content) {
                                       // Archive has been created
                                       _this.set('archive', content);
                                       _this.set(
                                           'archiveStatus',
                                           Config.FILE.ARCHIVE_STATUS.ARCHIVE_CREATED);
                                       _this._createUpload();
                                       
                                   }).catch(function() {
                                       // Archive can't be generated
                                       _this.set(
                                           'archiveStatus',
                                           Config.FILE.ARCHIVE_STATUS.ARCHIVE_FAILED);
                                       _this._createUpload();
                                   });
                        }
                    };
                    
                    reader.readAsArrayBuffer(dataFile);
                });
                
        } else {
            // No point attempting to archive file
            this.set('archiveStatus',
                     Config.FILE.ARCHIVE_STATUS.ARCHIVE_SKIPPED);
            this._createUpload();
        }        
    },

    /**
     * Create server upload object
     */
    _createUpload: function() {
        var _this = this,
            hasArchive = false,
            filesData;
        
        filesData = this.get('filesNoErrors').map(function(file) {
            return {hash_key: file.get('hashKey')};
        });
        
        if (filesData.length > 1 && !Ember.isBlank(this.get('archive'))) {
            hasArchive = true;
        }
                    
        ajax({
            url: 'uploads/',
            method: 'POST',
            data: {
                files: filesData,
                has_archive: hasArchive
            }
        }).then(function(result) {
            // Cache the upload object
            _this.set('uploadData', result);
            if (hasArchive) {
                _this._uploadArchive();
            } else {
                _this._completeTransfer();
            }
            
        }).catch(function() {
            _this._abortTransfer();
        });
    },

    /**
     * Upload archive zip to server that we promised the server we had ready for
     * an upload
     */
    _uploadArchive: function() {
        var _this = this,
            uploadData = this.get('uploadData'),
            archiveExpected = false,
            uploadCredentials,
            formData;
        
        // Upload object has been created on the server so check for
        // if we have an archive to upload or not
            
        // An archive is expected if the uploadData has meta data with
        // a presigned_post object
        if (uploadData && uploadData.meta &&
            uploadData.meta.presigned_post) {
                uploadCredentials = uploadData.meta.presigned_post;
                archiveExpected = true;
        }

        // If an archive is expected then we either start uploading this
        // archive or complete the transfer
        if (archiveExpected) {
            if (Ember.isBlank(this.get('archive'))) {
                // No archive to upload but one was expected so this counts
                // as a failure
                this._updateUpload(false);
                
            } else {
                formData = new FormData();
                for (var key in uploadCredentials.fields) {
                    formData.append(key, uploadCredentials.fields[key]);
                }
                formData.append('file', this.get('archive'));
                
                Ember.$.ajax({
                    url: uploadCredentials.url,
                    method: 'POST',
                    data: formData,
                    //prevent jQuery from automatically transforming
                    // data into a querystring
                    processData: false,
                    // Content type must be set to false, otherwise jQuery
                    // will set it incorrectly
                    contentType: false,
                    success: function() {
                        _this._updateUpload(true);
                    },
                    error: function() {
                        _this._updateUpload(false);
                    }
                });
            }
        } else {
            // An archive isn't expected of us then it's safe to say we
            // are done here
            this._completeTransfer();
        }

        
    },
    
    
    /**
     * Update server upload object to let it know if the archive was
     * successfully uploaded or not
     *
     * @param uploadedArchive indicator of if the archive was successfully
     *     uploaded
     */
    _updateUpload: function(uploadedArchive) {
        var _this = this,
            hashKey = this.get('uploadData').hash_key;
        
        ajax({
            url: `uploads/${hashKey}/`,
            method: 'PATCH',
            data: {
                uploaded_archive: uploadedArchive
            }
        }).catch(function() {
            // Wouldn't worry about a failure as the server will fix it up
            // now or upon download request
        }).finally(function() {
            _this._completeTransfer();
        });
    },


    /**
     * Abort the transfer as it has failed
     */
    _abortTransfer: function() {
        this.set('transferStatus', Config.TRANSFER_STATUS.FAILED);
        
    },
    
    /**
     * Complete the transfer.
     *
     * At this stage, the upload object has been created on the server
     * and any archive to be sent has been uploaded (hopefully).
     * 
     * Now is time to forward the upload emails if any
     */
    _completeTransfer: function() {
        this._validateEmailAndSend();
        this.set('transferStatus', Config.TRANSFER_STATUS.COMPLETED);
    },
        
    /**
     * Setup the fileupload plugin
     */
    _setupFileUploader: function() {
        var _this = this,
            uploadElement = this.$('.js-dropzone');
        
        uploadElement.each(function (index, item) {
            var $item = Ember.$(item);
            $item.fileupload({
                url: apiPath('files/'),
                type: 'POST',
                dropZone: $item,
                autoUpload: false,
            }
            ).on('fileuploadadd', Ember.run.bind(_this, _this._fileUploadAdd)
            ).on('fileuploadsend', Ember.run.bind(_this, _this._fileUploadSend)
            ).on('fileuploaddone', Ember.run.bind(_this, _this._fileUploadDone)
            ).on('fileuploadfail', Ember.run.bind(_this, _this._fileUploadFail)
            ).on('fileuploadprogress',
                 Ember.run.bind(_this, _this._fileUploadProgress)
            );
        });
    },
    
    /**
     * The add callback is invoked as soon as files are added to the fileupload
     * widget (via file selection, drag & drop or add API call).
     * 
     * @param e jquery event
     * @param data jquery file upload object
     */
    _fileUploadAdd: function(e, data) {
        var dataFile, file;
        if (e.isDefaultPrevented() || this.get('isTransferring')) {
            return;
        }
        
        // Create a store record for each file
        if (data && data.files && data.files.length) {
            // There should only be one file entry
            dataFile = data.files[0];
            file = this.get('store').createRecord('file', {
                name: dataFile.name.trim(),
                size: dataFile.size,
                contentType: dataFile.type,
                uploadId: Date.now(),
                uploadStatus: Config.FILE.STATUS.READY,
                fileUploadData: data
            });
            
            data.context = file;
        }
    },
    
    /**
     * Callback for the start of each file upload request
     * 
     * @param e jquery event
     * @param data jquery file upload object
     */
    _fileUploadSend: function(e, data) {
        if (e.isDefaultPrevented()) {
            return false;
        }

        var file = data.context;
        file.set('uploadStatus', Config.FILE.STATUS.UPLOADING);

        if (data.context && data.dataType &&
            data.dataType.substr(0, 6) === 'iframe') {
                // Iframe Transport does not support progress events.
                // In lack of an indeterminate progress bar, we set
                // the progress to 100%, showing the full animated bar:
                data.context.progress = 100;
        }
    },
    
    /**
     * Callback for successful upload requests
     *
     * @param e jquery event
     * @param data jquery file upload object with a jqXHR object
     */
    _fileUploadDone: function(e, data) {
        var file =  data.context;
        file.set('progress', 100);
        
        // File has been completely uploaded so inform the server of this
        file.set('uploadedFile', true);
        file.save().catch(function() {
            // Don't worry about this failing, when we create the
            // upload container this gets fixed up.
        }).finally(function() {
            file.set('uploadStatus', Config.FILE.STATUS.COMPLETED);
        });
    },
    
    /**
     * Callback for failed (aborted or error) upload requests
     *
     * @param e jquery event
     * @param data jquery file upload object with a jqXHR object
     */
    _fileUploadFail: function(e, data) {
        var file = data.context;
        file.set('uploadStatus', Config.FILE.STATUS.FAILED);
    },

    /**
     * Callback for upload progress events
     *
     * @param e jquery event
     * @param data jquery file upload object
     */
    _fileUploadProgress: function(e, data) {
        var progress = Math.floor(data.loaded / data.total * 100),
            file = data.context;
        file.set('progress', progress);
        
    },

    /**
     * If there's a valid email address then use it
     */
    _validateEmailAndSend: function() {
        var _this = this;
        
        if (Ember.isBlank(this.get('email'))) {
            return;
        }
        
        this.validate().then(function() {
            // all validations pass so can forward request to the server
            _this._sendEmail();
            
        }).catch(function() {
            // any validations fail
            
        }).finally(function() {
            // all validations complete regardless of _this.get('isValid')
            // state
        });
    },
    
    /**
     * Forward the transfer URL via email
     */
    _sendEmail: function() {
        var _this = this,
            hashKey = this.get('uploadData').hash_key,
            email = this.get('email');

        this.set('sendingEmail', true);
        
        ajax({
            url: `uploads/${hashKey}/forward/`,
            method: 'PUT',
            data: {
                emails: [{email: email}]
            }
        }).then(function() {
           _this.set('successEmail', email);
        }).catch(function() {
            // Wouldn't worry about a failure as the server will fix it up
            // now or upon download request
        }).finally(function() {
            _this.set('sendingEmail', false);

            // Now that the email has been sent, trigger saving of the email
            // for autocomplete purposes
            // Ref: http://stackoverflow.com/a/15634249
            Ember.$('#email-submit').click();
        });
    },
    
    actions: {
        /**
         * Cancel upload if uploading and remove file from the store
         * 
         * @param file object to remove
         */
        remove: function(file) {
            var fileUploadData = file.get('fileUploadData'),
                uploadStatus = file.get('uploadStatus'),
                hashKey = file.get('hashKey');

            if (uploadStatus === Config.FILE.STATUS.UPLOADING &&
                fileUploadData.abort) {
                    fileUploadData.abort();
            }
            
            // Calling file.destroyRecord() on server-persisted files would
            // be the easy way out here but that is actually slow as the
            // object wouldn't be unloaded from the store until the delete
            // is completed server-side. Which is a problem because
            // a user shouldn't get bogged down by this operation.
            file.unloadRecord();
            if (!file.get('isNew') && hashKey) {
                ajax({
                    url: `files/${hashKey}/`,
                    method: 'DELETE'
                }).catch(function() {
                    // No big deal if this fails. The server cleans it up later.
                });
            }
        },

        /**
         * Start transfer as soon as all files are uploaded
         */
        startTransfer: function() {
            this.set('transferStatus', Config.TRANSFER_STATUS.STARTED);
        }
    }
});
