import Ember from 'ember';
import RouteResetScroll from 'raven/mixins/route-reset-scroll';

export default Ember.Route.extend(RouteResetScroll, {
    title: 'Raven - Send files faster than lightning',

    /**
     * The store service
     */
    store: Ember.inject.service('store'),

    /**
     * The model is all files that are already loaded in the database
     */
    model: function() {
        return this.get('store').peekAll('file');
    },
});
