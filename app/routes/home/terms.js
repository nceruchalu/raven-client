import Ember from 'ember';
import RouteResetScroll from 'raven/mixins/route-reset-scroll';

export default Ember.Route.extend(RouteResetScroll, {
    titleToken: 'Terms of Service'
});
