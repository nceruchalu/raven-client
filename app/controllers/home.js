import Ember from 'ember';

export default Ember.Controller.extend({
    /**
     * Current year for the copyright
     */
    currentYear: Ember.computed(function() {
        var date = new Date();
        return date.getFullYear();
    }),


    actions: {
        /**
         * Collapse the navigation bar
         */
        collapseNavbar: function() {
            Ember. $(".navbar-collapse").collapse('hide');
        }
    }
});