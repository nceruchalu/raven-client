/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app'),
    environment = EmberApp.env(),
    isProduction = environment === 'production';

module.exports = function(defaults) {
    var app = new EmberApp(defaults, {
        // Add options here
        // Ref: http://www.ember-cli.com/user-guide/#asset-compilation
        fingerprint: {
            enabled: isProduction,
            extensions: ['js', 'css', 'png', 'jpg', 'gif', 'map', 'ico', 'svg',
                         'json', 'mp4'],
            replaceExtensions: ['html', 'js', 'css', 'xml', 'json'],
            prepend: 'https://raven-static.s3.amazonaws.com/'
        },
        
        compassOptions: {
            outputStyle: isProduction ? 'compressed' : 'expanded'
        }
    });

    // Use `app.import` to add additional libraries to the generated
    // output files.
    //
    // If you need to use different assets in different
    // environments, specify an object as the first parameter. That
    // object's keys should be the environment name and the values
    // should be the asset to use in that environment.
    //
    // If the library that you are including contains AMD or ES6
    // modules that you would like to import into your application
    // please specify an object with the list of modules as keys
    // along with the exports of each module as its value.

    // Stylesheets to be concatenated to vendor.css
    app.import('vendor/css/normalize.css');
    app.import('vendor/css/ie10-viewport-bug-workaround.css');
    app.import('bower_components/bootstrap/dist/css/bootstrap.min.css');
    app.import('bower_components/font-awesome/css/font-awesome.min.css');
    
    // Add font awesome fonts
    app.import('bower_components/font-awesome/fonts/FontAwesome.otf', {
        destDir: 'fonts'
    });
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.eot', {
        destDir: 'fonts'
    });
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.svg', {
        destDir: 'fonts'
    });
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.ttf', {
        destDir: 'fonts'
    });
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.woff', {
        destDir: 'fonts'
    });
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.woff2',{
        destDir: 'fonts'
    });
    
    // Scripts to be concatenated to vendor.js
    app.import('vendor/js/ie10-viewport-bug-workaround.js');
    app.import('bower_components/bootstrap/dist/js/bootstrap.min.js');
    app.import('bower_components/clipboard/dist/clipboard.min.js');
    app.import('bower_components/jszip/dist/jszip.min.js');
    
    // jquery-fileupload and its dependencies
    app.import(
        'bower_components/jquery-file-upload/js/vendor/jquery.ui.widget.js');
    app.import(
        'bower_components/jquery-file-upload/js/jquery.iframe-transport.js');
    app.import('bower_components/jquery-file-upload/js/jquery.fileupload.js');
    
    return app.toTree();
};
