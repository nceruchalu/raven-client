import Ember from 'ember';
import FormSubmissionMixin from 'raven/mixins/form-submission';
import { module, test } from 'qunit';

module('Unit | Mixin | form submission');

// Replace this with your real tests.
test('it works', function(assert) {
  let FormSubmissionObject = Ember.Object.extend(FormSubmissionMixin);
  let subject = FormSubmissionObject.create();
  assert.ok(subject);
});
