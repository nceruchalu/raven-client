import { applyInflection } from 'raven/helpers/apply-inflection';
import { module, test } from 'qunit';

module('Unit | Helper | apply inflection');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = applyInflection([42]);
  assert.ok(result);
});
