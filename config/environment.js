/* jshint node: true */

module.exports = function(environment) {
    var ENV = {
        modulePrefix: 'raven',
        environment: environment,
        rootURL: '/',
        locationType: 'auto',
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary 
                // build e.g. 'with-controller': true
            }
        },

        APP: {
            // Here you can pass flags/options to your application instance
            // when it is created
        },

        
        FILE: {
            // File model upload status flags
            STATUS: {
                READY: 0,              // Just created the file object
                DUPLICATE_NAME: 1,     // Duplicate name in upload group
                LONG_NAME: 2,          // Long file name
                LARGE: 3,              // File is too large to upload
                MAKES_TOTAL_LARGE: 4,  // File's size makes sum total too large
                EMPTY: 5,              // Zero bytes
                CREDENTIALS_FAILED: 6, // Unable to get S3 upload credentials
                HAS_CREDENTIALS: 7,    // Successfully got S3 upload credentials
                UPLOADING: 8,          // uploading file
                FAILED: 9,             // File upload failed
                COMPLETED: 10,         // File upload completed successfully
            },

            // Archive creation status flags
            ARCHIVE_STATUS: {
                NO_ARCHIVE: 0,        // No archive considered for creation yet
                CREATING_ARCHIVE: 1,  // Attemping to create archive
                ARCHIVE_SKIPPED: 2,   // Archive too large to bother creating
                ARCHIVE_FAILED:  3,   // Archive creation failed
                ARCHIVE_CREATED: 4,   // Archive creation succeeded
                UPLOADING_ARCHIVE: 5, // Uploading archive
                ARCHIVE_UPLOAD_FAILED: 6, // Archive upload failed
                ARCHIVE_COMPLETED: 7, // Archive upload completed successfully
            },
                        
            // Max size in bytes
            MAX_SIZE: 2*1024*1024*1024,
            
            // Max total size of all files in bytes
            MAX_TOTAL_SIZE: 2*1024*1024*1024,
            
            // Max name length
            MAX_NAME_LENGTH: 250,

            // Max total file size, in bytes, for client-side archiving
            MAX_TOTAL_ARCHIVE_SIZE: 100*1024*1024,
        },

        // Transfer status flags
        TRANSFER_STATUS: {
            INACTIVE: 0,  // Transfer hasn't started yet
            STARTED: 1,   // User has requested transfer process start
            FAILED: 2,    // The transfer failed
            COMPLETED: 3  //  Transfer is completed
        }
        
    };
    
    ENV.contentSecurityPolicy = {
        'default-src': "'self' checkout.stripe.com",
        
        // Allow scripts from Google Analytics, Stripe and Same Origin
        // Use unsafe-eval for sake of Firefox
        // Ref: https://github.com/emberjs/ember-inspector/issues/471
        'script-src': "'self' 'unsafe-inline' " +
                      "https://www.google-analytics.com " +
                      "https://checkout.stripe.com *.localhost:49154 " +
                      "*.s3.amazonaws.com *.cloudfront.net " + 
                       (environment === 'development' ? "'unsafe-eval'" : ""),
        
        // Allow fonts to be loaded from fonts.gstatic.com
        'font-src': "'self' http://fonts.gstatic.com " +
                    "https://fonts.gstatic.com *.s3.amazonaws.com " +
                    "*.cloudfront.net",
        
        // Allow data (ajax/websocket) from .tryraven.com and stripe
        // and upload to amazon S3
        'connect-src': "'self' tryraven.com *.tryraven.com " +
                       "checkout.stripe.com *.s3.amazonaws.com " +
                       "localhost:8000 *.localhost:8000 ws://*.localhost:49154",
        
        // Allow images from S3 and (apparently?!) Google Analytics
        // Allow images from anywhere to accomodate syndication feeds' notes
        'img-src': "'self' www.google-analytics.com *.s3.amazonaws.com " + 
                   "http://*.cloudfront.net https://*.cloudfront.net " + 
                   "https://q.stripe.com data: *",
        
        // Allow inline styles and loaded CSS from fonts.googleapis.com, stripe
        'style-src': "'self' 'unsafe-inline' http://fonts.googleapis.com " +
                     "https://fonts.googleapis.com " +
                     "https://checkout.stripe.com *.s3.amazonaws.com " +
                     "*.cloudfront.net",
        
        'child-src': "'self' https://checkout.stripe.com",
        
        'media-src': "'self'"
    };
    
    ENV.APP.API_NAMESPACE = 'api/v1';
    
    ENV.APP.APP_NAME = 'Raven';
    
    if (environment === 'development') {
        // ENV.APP.LOG_RESOLVER = true;
        // ENV.APP.LOG_ACTIVE_GENERATION = true;
        ENV.APP.LOG_TRANSITIONS = true;
        // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        // ENV.APP.LOG_VIEW_LOOKUPS = true;
        
        // REST API host
        ENV.APP.API_HOST = 'http://localhost:8000';
        
        // Webserver host
        ENV.APP.WEB_HOST = 'http://localhost:8000';
        
        // Webserver protocol
        ENV.APP.WEB_PROTOCOL = 'http://';
    }

    if (environment === 'test') {
        // Testem prefers this...
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
    }

    if (environment === 'production') {
        // REST API host
        ENV.APP.API_HOST = 'https://tryraven.com';
        
        // Webserver host
        ENV.APP.WEB_HOST = 'https://tryraven.com';
    }

    return ENV;
};
